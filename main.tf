module "vpc" {
  source = "./modules/vpc"
  vpc_cidr= var.vpc_cidr
  vpc_name=var.vpc_name
}

module "private_subnet" {
    source = "./modules/private_subnet"
    vpc_id = ["${module.vpc.dev_vpc_id}", "${module.vpc.qa_vpc_id}", "${module.vpc.prod_vpc_id}" ]
    private_subnet_cidr = var.private_subnet_cidr
    azs=var.azs
}
module "public_subnet" {
    source = "./modules/public_subnet"
    vpc_id = ["${module.vpc.dev_vpc_id}", "${module.vpc.qa_vpc_id}", "${module.vpc.prod_vpc_id}" ]
    public_subnet_cidr = var.public_subnet_cidr
    azs=var.azs
}

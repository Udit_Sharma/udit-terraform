#VPC_vars
variable "vpc_cidr" {
    type=list
    default = ["10.11.0.0/16","10.22.0.0/16","10.33.0.0/16"]
}

variable "vpc_name" {
    type=list
    default = ["dev","qa","prod"]
}
variable "azs" {
    type=list
    default = ["ap-south-1a","ap-south-1b","ap-south-1c"]
}
variable "private_subnet_cidr" {
    type=list
    default = ["10.11.1.0/24","10.22.1.0/24","10.33.1.0/24"]
}

variable "public_subnet_cidr" {
    type=list
    default = ["10.11.101.0/24","10.22.101.0/24","10.33.101.0/24"]
}
 

# #StorageAccount
# variable "storage_account_name" {}
# variable "storage_account_tier" {}
# #PrivateLinkService
# variable "private_link_service_name" {}
# variable "primary_private_ip_addr"{}
# #PrivateEndpoint
# variable "private_endpoint_name" {}
# #Networking
# variable "vnet_name" {}
# variable "vnet_address_space" {}
# variable "subnet_name" {}
# variable "subnet_address_prefixes" {}
# variable "nat_gateway_name" {}
# variable "lb_name" {}
# variable "lb_location" {}
# variable "load_balancer_public_ip_name"{}

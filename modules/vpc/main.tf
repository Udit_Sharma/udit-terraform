resource "aws_vpc" "myvpc" {
  count=length(var.vpc_name)
  cidr_block = element(var.vpc_cidr , count.index)

  tags = {
    Name = element(var.vpc_name , count.index)
  }
}

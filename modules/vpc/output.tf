output "dev_vpc_id" {
value = "${aws_vpc.myvpc[0].id}"
}

output "qa_vpc_id" {
    value = "${aws_vpc.myvpc[1].id}"
}

output "prod_vpc_id" {
    value = "${aws_vpc.myvpc[2].id}"
}

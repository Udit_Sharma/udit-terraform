variable "vpc_cidr" {
    type=list
    default = ["10.1.0.0/16","10.2.0.0/16","10.3.0.0/16"]
}

variable "vpc_name" {
    type=list
    default = ["dev","qa","prod"]
}
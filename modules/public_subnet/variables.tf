variable "vpc_id" {}
variable "public_subnet_cidr" {
    type=list
    default = ["10.1.101.0/24","10.2.101.0/24","10.3.101.0/24"]
}
variable "azs" {
    type=list
    default = ["ap-south-1a","ap-south-1b","ap-south-1c"]
}
resource "aws_subnet" "public_subnet" {
  count = length(var.public_subnet_cidr)
  vpc_id  = element(var.vpc_id , count.index)
  cidr_block = element(var.public_subnet_cidr , count.index)
  availability_zone = element(var.azs , count.index)
  map_public_ip_on_launch = true
  tags = {
    Name = "Pub_Subnet_vpc${count.index+1}"
  }
}
resource "aws_subnet" "private_subnet" {
  count = length(var.private_subnet_cidr)
  vpc_id  = element(var.vpc_id , count.index)
  cidr_block = element(var.private_subnet_cidr , count.index)
  availability_zone = element(var.azs , count.index)
  tags = {
    Name = "Pvt_Subnet_vpc${count.index+1}"
  }
}
#VPC_vars
variable "vpc_id" {}
variable "private_subnet_cidr" {
    type=list
    default = ["10.1.1.0/24","10.2.1.0/24","10.3.1.0/24"]
}
variable "azs" {
    type=list
    default = ["ap-south-1a","ap-south-1b","ap-south-1c"]
}